import React from 'react';
import DataTable from 'react-data-table-component';
import { apiGet, apiPost} from '../ApiHelper'
import { Button, Modal, Form } from 'react-bootstrap';

class CustomerAccount extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            show: false,
            row: {},
            validated: false,
            parent: true,
            child: false,
            parentId: -1,
            childData: []
        };

    }

    async componentDidMount() {

    }

    loadData = async () => {
        const resp = await apiGet(`api/Customer/GetCustomersAsync/${this.props.parentId}`);
        return resp.data.accounts;
    }

    handleClose = () => {
        this.setState({ show: false, errorMessage: null })
    }

    handleOpen = async (id) => {
        let newRow = { Id: 0, CustomerID: this.props.parentId }; 
        this.setState({ row: newRow, show: true, validated: false, errorMessage: null });
    }

    handleChange = (key, value) => {
        let currentRow = this.state.row;
        currentRow[key] = value;
        this.setState({ row: currentRow });
    }

    handleSave = async (event) => {
        const { t } = this.props;
        event.preventDefault();
        event.stopPropagation();
        const form = event.currentTarget;
        if (form.checkValidity() === false) {

            this.setState({ validated: true });
            return;
        }


        let currentRow = this.state.row;
        let resp = null;
        var errorMessage = '';
        resp = await apiPost('api/ClientAccounts', currentRow);
        if (resp.data.Message && resp.data.Message === "Error") {
            this.setState({ errorMessage: errorMessage });
            return;
        }
        let data = await this.loadData();
        this.setState({ data: data, show: false });
    }

    render() {
        let columns = [
            {
                name: 'Id',
                selector: row => row.id
            },
            {
                name: 'Balance',
                selector: row => row.balance
            }
        ];
        let transactionColumns=[
            {
                name: 'Id',
                selector: row => row.id
            },
            {
                name: 'Amount',
                selector: row => row.amount
            },
            {
                name: 'Type',
                selector: row => row.tranType,
                cell:(row => row.tranType == 0 ? 'Credit' : 'Debit')
            }
        ];
        const ExpandedComponent = ({ data }) => <DataTable
                                    columns={transactionColumns}
                                    data={ data.transactions}
                                    ></DataTable>;
        return (
            <>
                <DataTable
                    title={(<Button variant="link" onClick={this.props.backToPrevious}>{`${this.props.parentName} - `}Accounts</Button>)}
                    highlightOnHover
                    columns={columns}
                    data={this.state.data ? this.state.data : this.props.data}
                    actions={
                        // <IconButton color="primary" onClick={() => this.handleOpen()}>
                        //     <Add />
                        // </IconButton>
                        <Button onClick={() => this.handleOpen()}>Add</Button>
                    }
                    expandableRows={true}
                    expandableRowsComponent={ExpandedComponent}
                />
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add</Modal.Title>
                    </Modal.Header>
                    <Form noValidate validated={this.state.validated} onSubmit={this.handleSave}>
                        <Modal.Body>
                            {!!this.state.errorMessage ? <div style={{ color: '#dc3545' }}><ul dangerouslySetInnerHTML={{ __html: this.state.errorMessage }}></ul></div> : null}
                            <Form.Label>Initial Credit</Form.Label>
                            <Form.Control type="Text" required placeholder={"Initial Credit"} Name="InitialCredit" defaultValue={this.state.row.Name}
                                onChange={e => this.handleChange('InitialCredit', e.target.value)} />

                        </Modal.Body>

                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.handleClose}>
                                Close
                            </Button>
                            <Button type="submit" variant="primary">Save Changes</Button>
                        </Modal.Footer>
                    </Form>
                </Modal>

            </>
        );
    }
}

export default CustomerAccount;