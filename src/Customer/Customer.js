import React from 'react';
import DataTable from 'react-data-table-component';
import {apiGet, apiPost} from '../ApiHelper'
import { Button, Collapse } from 'react-bootstrap';
import CustomerAccount from './CustomerAccount';

class Customer extends React.Component{

    constructor(props) {
		super(props);
		this.state = {
			data: [],
			show: false,
			row: {},
			validated: false,
			parent: true, 
			child: false , 
			parentId : -1,
			childData: []
		};

	}

    async componentDidMount() {
		let data = await this.loadData();

		this.setState({ data: [data] });
	}

    loadData = async () => {
		const resp = 
        await apiGet('api/Customer/GetCustomersAsync/1');
		return resp.data;
	}

    loadChilds = async (parentId) => {
		const {data} = this.state;
		return data.find(element => element.id == parentId).accounts;
	}

    openChilds = async (row) =>{
		let childData = await this.loadChilds(row.id);
		this.setState({ 
			child: !this.state.child, 
			parent: !this.state.parent, 
			parentId: row.id, 
			childData: childData,
			parentName : row.name + ' ' + row.surname
		});
	}

    backToPrevious = () =>{
		this.setState({ 
			parent: !this.state.parent, 
			child: !this.state.child, 
			parentId: -1, 
			childData: [],
		});
	}


    render() {
		let columns = [
			{
				name: 'Name',
				selector: row => row.name ,
			},
			{
				name: 'Surname',
				selector: row => row.surname ,
			},
            {
                cell: row =><Button variant="primary" onClick={() => this.openChilds(row)}
					     aria-controls="child-collapse" >
        				Accounts
      					</Button>,
				width: "10rem"
            }
		];
		return (
			< div>
				<Collapse in={this.state.parent} dimension="width">
					<div id="child-collapse">
						<DataTable
							title='Customers'
							highlightOnHover
							columns={columns}
							data={this.state.data}
						/>
					</div>
				</Collapse>
				<Collapse in={this.state.child} dimension="width">
					<div id="child-collapse">
					{ <CustomerAccount parentId={this.state.parentId} data={this.state.childData} 
							parentName={this.state.parentName} backToPrevious ={this.backToPrevious}/> }
					</div>
				</Collapse>
				
			</div>
		);
	}
}

export default Customer;