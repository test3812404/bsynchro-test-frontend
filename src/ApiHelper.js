
import axios from 'axios';


//const apiUrl = 'http://localhost:9999/api'
const apiUrl = 'http://192.168.1.105:5000'

export async function apiGet(url) {
  const resp = await axios.get(`${apiUrl}/${url}`,
  {
    headers:
    {
      'Access-Control-Allow-Origin': '*'
    }
  });
//   const resp = {
//     data : {
//         Data : [
//             { Name: 'Hiba' , Surname: 'Totonji'},
//             { Name: 'Imad' , Surname: 'Totonji'}
//         ]
//     }
//  }
  return resp;
}

export async function apiPost(url, data) {
  const resp = await axios.post(`${apiUrl}/${url}`, data,
  {
    headers:
    {
      'Access-Control-Allow-Origin': '*'
    }
  });
  return resp;
}

export async function apiPut(url, data) {
  const resp = await axios.put(`${apiUrl}/${url}`,data,
  {
    headers:
    {
      'Access-Control-Allow-Origin': '*'
    }
  });
  return resp;
}

export async function apiDelete(url, data) {
  const resp = await axios.delete(`${apiUrl}/${url}`,
  {
    headers:
    {
      'Access-Control-Allow-Origin': '*'
    }
  });
  return resp;
}